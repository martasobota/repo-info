# repo-info
## Details of GitHub repository

The goal of the repository is creating REST API service, which will return information about GitHub repository depending on the passed `owner` and `repository-name`.

The structure of the endpoint:
```
GET /repositories/{owner}/{repository-name}
```
Example response:
```
{
  "full_name": "...",
  "description": "...",
  "clone_url": "...",
  "stars": 0,
  "created_at": "..."
}
```
