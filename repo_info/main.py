# """
# Repo Info API
# """

from flask import Flask


app = Flask(__name__)


@app.route("/")
def check():
    """Check if app is alive"""
    return "It's working!"


if __name__ == '__main__':
    app.run()
